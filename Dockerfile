FROM alpine:3

RUN apk update && \
    apk add curl openjdk11-jre graphviz ttf-dejavu

WORKDIR /build
 
ENV HOME /build

RUN curl --output-dir  /build -JLO http://sourceforge.net/projects/plantuml/files/plantuml.jar/download

USER 1000:1000

WORKDIR /work

ENTRYPOINT ["/bin/sh", "-xvc", "java -Djava.awt.headless=true -jar /build/plantuml.jar -overwrite -tpng $@", "$@"]
